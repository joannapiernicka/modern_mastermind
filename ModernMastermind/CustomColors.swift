//
//  CustomColors.swift
//  MastermindGame
//
//  Created by P Z on 23.03.2017.
//  Copyright © 2017 Joanna Charysz-Piernicka. All rights reserved.
//

import UIKit

class CustomColors: UIColor {
    
    public static let cRed = UIColor(red: 255/255, green: 59/255, blue: 48/255, alpha: 1) //red
    public static let cOrange = UIColor(colorLiteralRed: 255/255, green: 149/255, blue: 0/255, alpha: 1) //orange
    public static let cYellow = UIColor(colorLiteralRed: 255/255, green: 204/255, blue: 0/255, alpha: 1) //yellow
    public static let cGreen = UIColor(colorLiteralRed: 76/255, green: 217/255, blue: 100/255, alpha: 1) //green
    public static let cTealBlue = UIColor(colorLiteralRed: 90/255, green: 200/255, blue: 250/255, alpha: 1) //teal blue
    public static let cBlue = UIColor(colorLiteralRed: 0/255, green: 122/255, blue: 255/255, alpha: 1) //blue
    public static let cPurple = UIColor(colorLiteralRed: 88/255, green: 86/255, blue: 214/255, alpha: 1) //purple
    public static let cPink = UIColor(colorLiteralRed: 252/255, green: 145/255, blue: 254/255, alpha: 1) //pink
    public static let cGrey = UIColor(colorLiteralRed: 188/255, green: 188/255, blue: 188/255, alpha: 1)  //grey
    public static let cBrown = UIColor(colorLiteralRed: 165/255, green: 128/255, blue: 101/255, alpha: 1)  //brown
    
    
    
}
