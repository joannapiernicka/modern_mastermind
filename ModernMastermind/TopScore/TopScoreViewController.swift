//
//  TopScoreViewController.swift
//  ModernMastermind
//
//  Created by P Z on 09.05.2017.
//  Copyright © 2017 Joanna Charysz-Piernicka. All rights reserved.
//

import UIKit

class TopScoreViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    var object: [Any]?
    let cellIndentifier = "topScoreCell"
    
    let userDataManager = UserDataManager()
    
    @IBOutlet weak var topScoreTable: UITableView!
    
    @IBAction func deleteAllButton(_ sender: Any) {
        userDataManager.deleteAllUserResultFromCoreData()
        topScoreTable.reloadData()
    }
    
    override func viewDidLoad() {
        userDataManager.fetchUserResultFromCoreData()
        super.viewDidLoad()
        self.topScoreTable.delegate = self
        self.topScoreTable.dataSource = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK: topScoreTableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userDataManager.players.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIndentifier, for: indexPath) as? TopScoreTableViewCell
        let player = userDataManager.players[indexPath.row]
        cell?.userNameLabel.text = player.value(forKeyPath: "name") as? String
        cell?.userTimeLabel.text = player.value(forKeyPath: "timeScore") as? String
        return cell!
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            object?.remove(at: indexPath.row)
            userDataManager.deleteSingleUserResultFromCoreData(index: indexPath.row)
            userDataManager.fetchUserResultFromCoreData()
            tableView.deleteRows(at: [indexPath], with: .fade )
        }
    }
    
}
