//
//  TopScoreTableViewCell.swift
//  ModernMastermind
//
//  Created by P Z on 17.05.2017.
//  Copyright © 2017 Joanna Charysz-Piernicka. All rights reserved.
//

import UIKit

class TopScoreTableViewCell: UITableViewCell {
    

    @IBOutlet weak var userNameLabel: UILabel!
    
    
    @IBOutlet weak var userTimeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
