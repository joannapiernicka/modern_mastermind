//
//  ColorsCustomLabel.swift
//  ModernMastermind
//
//  Created by P Z on 30.04.2017.
//  Copyright © 2017 Joanna Charysz-Piernicka. All rights reserved.
//


import UIKit

protocol ColorLabelProtocol:class {
    func labelTap(label:ColorsCustomLabel)
}

class ColorsCustomLabel: UILabel {
    weak var delegate:ColorLabelProtocol?
    
    var indexPath:IndexPath?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let tap = UITapGestureRecognizer()
        tap.numberOfTapsRequired = 1
        tap.addTarget(self, action: #selector(labelTap))
        self.isUserInteractionEnabled = true
        
        self.textColor = UIColor.lightGray
        
        self.layer.cornerRadius = 8
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 1
    }
    dynamic func labelTap() {
        delegate?.labelTap(label: self)
        
    }
}

