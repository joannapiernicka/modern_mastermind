//
//  ColorsTableViewCell.swift
//  ModernMastermind
//
//  Created by P Z on 30.04.2017.
//  Copyright © 2017 Joanna Charysz-Piernicka. All rights reserved.
//

import UIKit

class ColorsTableViewCell: UITableViewCell {

    @IBOutlet weak var turnLabel: UILabel!
    
    @IBOutlet weak var Index0TableLabel: ColorsCustomLabel!
    
    @IBOutlet weak var Index1TableLabel: ColorsCustomLabel!
    
    @IBOutlet weak var Index2TableLabel: ColorsCustomLabel!
    
    @IBOutlet weak var Index3TableLabel: ColorsCustomLabel!
    
    @IBOutlet weak var placeAndColourOkLabel: UILabel!
    
    @IBOutlet weak var onlyColourOkLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
