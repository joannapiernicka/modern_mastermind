//
//  ViewController.swift
//  ModernMastermind
//
//  Created by P Z on 30.04.2017.
//  Copyright © 2017 Joanna Charysz-Piernicka. All rights reserved.
//

import UIKit



class ViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate,UIPickerViewAccessibilityDelegate, UITableViewDelegate, UITableViewDataSource {
    
    var rowFromPicker0: Int = 0
    var rowFromPicker1: Int = 0
    var rowFromPicker2: Int = 0
    var rowFromPicker3: Int = 0
    
    var textFromPicker0: String = "red"
    var textFromPicker1: String = "red"
    var textFromPicker2: String = "red"
    var textFromPicker3: String = "red"
    
    var savedPlayersArrayAndTurn: [Int : [String]] = [:]
    var savedCheckResult: [ Int: (placeAndColourOK: Int, onlyColorOk: Int)] = [0: (0, 0)]
    
    var gameModel = GameModel()
    let userDataManager = UserDataManager()
    
    enum insertNewRowError : Error {
        case endGame
        case noFreeRowInArray
        case otherError
    }
    

    @IBAction func StartButton(_ sender: Any) {
        showPlayStartAlert()
    }
    
    @IBAction func TopScoreButton(_ sender: Any) {
    }
    
    @IBOutlet weak var correctAnsw0Label: ColorsCustomLabel!
    
    @IBOutlet weak var correctAnsw1Label: ColorsCustomLabel!
   
    @IBOutlet weak var correctAnsw2Label: ColorsCustomLabel!
    
    @IBOutlet weak var correctAnsw3Label: ColorsCustomLabel!
    
    @IBOutlet weak var time: UILabel!
   
    @IBOutlet weak var colorsTable: UITableView!
    
    @IBOutlet weak var Index0Picker: UIPickerView!
    
    @IBOutlet weak var Index1Picker: UIPickerView!
    
    @IBOutlet weak var Index2Picker: UIPickerView!
 
    @IBOutlet weak var Index3Picker: UIPickerView!
    
    @IBOutlet weak var checkButt: UIButton!
   
    
    @IBAction func check(_ sender: Any) {
        sendTextFromPickerToPlayersArray()
        gameModel.check(GameModel.coloursPlayersArray)
        do {
            try savePlayersArrayAndActuallyTurn()
            try saveCheckResult()
            try insertNewObject(sender: Any.self)
        } catch insertNewRowError.endGame {
            print("gra się już skonczyła, rozpocznij nowa gre")
        } catch insertNewRowError.noFreeRowInArray {
            print("nie masz więcej ruchów, rozpocznij nową grę")
        } catch _ {
            print("nieznany błąd")
        }
        colorsTable.reloadSections([0], with: .automatic)
        showStateGameAlert()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.Index0Picker.dataSource = self
        self.Index1Picker.dataSource = self
        self.Index2Picker.dataSource = self
        self.Index3Picker.dataSource = self
        
        self.Index0Picker.delegate = self
        self.Index1Picker.delegate = self
        self.Index2Picker.delegate = self
        self.Index3Picker.delegate = self
        
        self.colorsTable.dataSource = self
        self.colorsTable.delegate = self
        
        self.checkButt.layer.cornerRadius = 8
        colorsTable.reloadData()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        prepareNewGame()
        
    }
    
    
    
    //MARK: picker view
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return GameModel.codeLenght
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return GameModel.colours.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return GameModel.colours[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        rowFromPicker0 = pickerView.selectedRow(inComponent: 0)
        rowFromPicker1 = pickerView.selectedRow(inComponent: 1)
        rowFromPicker2 = pickerView.selectedRow(inComponent: 2)
        rowFromPicker3 = pickerView.selectedRow(inComponent: 3)

        textFromPicker0 = GameModel.colours[rowFromPicker0]
        textFromPicker1 = GameModel.colours[rowFromPicker1]
        textFromPicker2 = GameModel.colours[rowFromPicker2]
        textFromPicker3 = GameModel.colours[rowFromPicker3]
    }
    
    
    //MARK: set background color for row in picker view
    
    func pickerView(_ pickerView: UIPickerView, viewForRow: Int, forComponent: Int, reusing: UIView?) -> UIView  {
        
        let pickerLabel = UILabel()
        pickerLabel.layer.cornerRadius = 10
        
        switch viewForRow {
        case 0:
            pickerLabel.layer.backgroundColor = CustomColors.cRed.cgColor
        case 1:
            pickerLabel.layer.backgroundColor = CustomColors.cOrange.cgColor
        case 2:
            pickerLabel.layer.backgroundColor = CustomColors.cYellow.cgColor
        case 3:
            pickerLabel.layer.backgroundColor = CustomColors.cGreen.cgColor
        case 4:
            pickerLabel.layer.backgroundColor = CustomColors.cTealBlue.cgColor
        case 5:
            pickerLabel.layer.backgroundColor = CustomColors.cBlue.cgColor
        case 6:
            pickerLabel.layer.backgroundColor = CustomColors.cPurple.cgColor
        case 7:
            pickerLabel.layer.backgroundColor = CustomColors.cPink.cgColor
        case 8:
            pickerLabel.layer.backgroundColor = CustomColors.cGrey.cgColor
        case 9:
            pickerLabel.layer.backgroundColor = CustomColors.cBrown.cgColor
        default:
            pickerLabel.layer.backgroundColor = UIColor.clear.cgColor
        }
        return pickerLabel
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    
    
    //MARK: TableView
    let cellIndentifier = "ColorsTVCellId"
    var objects = [Any]()

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objects.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIndentifier, for: indexPath) as! ColorsTableViewCell
        
        let savedPAATDictKeySorted = Array(savedPlayersArrayAndTurn.keys).sorted(by: >)
        let turn = savedPAATDictKeySorted[indexPath.row]
        cell.turnLabel.text = "\(turn)"
        cell.Index0TableLabel.layer.backgroundColor = printColor(array: savedPlayersArrayAndTurn[turn]!, index: 0).cgColor
        cell.Index1TableLabel.layer.backgroundColor = printColor(array: savedPlayersArrayAndTurn[turn]!, index: 1).cgColor
        cell.Index2TableLabel.layer.backgroundColor = printColor(array: savedPlayersArrayAndTurn[turn]!, index: 2).cgColor
        cell.Index3TableLabel.layer.backgroundColor = printColor(array: savedPlayersArrayAndTurn[turn]!, index: 3).cgColor
        cell.placeAndColourOkLabel.text = "\(String(describing: savedCheckResult[turn]!.placeAndColourOK))"
        cell.onlyColourOkLabel.text = "\(String(describing: savedCheckResult[turn]!.onlyColorOk))"
        print("actually turn \(gameModel.actuallyTurn)")
        return cell
    }
    
    // Insert new row to tableview
    func insertNewObject(_ sender: Any) throws {
        guard gameModel.actuallyTurn <= GameModel.allTurns else {throw insertNewRowError.noFreeRowInArray}
        guard gameModel.isPlayerWinGame() != GameModel.GameState.youWin else {throw insertNewRowError.endGame}
        
        objects.insert(ColorsTableViewCell(), at: 0)
        let indexPath = IndexPath(row: 0, section: 0)
        self.colorsTable.insertRows(at: [indexPath], with: .automatic)
    }
   
    func sendTextFromPickerToPlayersArray() {
        if GameModel.coloursPlayersArray.isEmpty != true {
            GameModel.coloursPlayersArray.removeAll()
        }
        GameModel.coloursPlayersArray.insert(textFromPicker0, at: 0)
        GameModel.coloursPlayersArray.insert(textFromPicker1, at: 1)
        GameModel.coloursPlayersArray.insert(textFromPicker2, at: 2)
        GameModel.coloursPlayersArray.insert(textFromPicker3, at: 3)
        
        print(GameModel.coloursPlayersArray) //testowo
    }
    
    func savePlayersArrayAndActuallyTurn() throws {
        guard gameModel.actuallyTurn <= GameModel.allTurns else {throw insertNewRowError.noFreeRowInArray}
        guard gameModel.isPlayerWinGame() != GameModel.GameState.youWin else {throw insertNewRowError.endGame}
        savedPlayersArrayAndTurn[gameModel.actuallyTurn] = GameModel.coloursPlayersArray
        print("savedPlayersArray: \(savedPlayersArrayAndTurn)")
    }
    
    func saveCheckResult() throws {
        guard gameModel.actuallyTurn <= GameModel.allTurns else {throw insertNewRowError.noFreeRowInArray}
        guard gameModel.isPlayerWinGame() != GameModel.GameState.youWin else {throw insertNewRowError.endGame}
        savedCheckResult[gameModel.actuallyTurn] = (gameModel.placeAndColourOk, gameModel.onlyColourOk)
        print("savedCheckResult: \(savedCheckResult)")
    }
    
    // MARK: Timer
    
    var timer = Timer()
    var intCounter = 0
    
    
    func updateCountdown() {
        intCounter += 1
        time.text! = String(format: "%02d:%02d:%02d", intCounter / 3600, (intCounter % 3600) / 60, (intCounter % 3600) % 60)
    }
    
    func resetTimer() {
        timer.invalidate()
        time.text = "00:00:00"
        intCounter = 0
    }
    
    func stopTimer() {
        timer.invalidate()
    }
    
    //MARK: Prepare new game
    
    func prepareNewGame() {
        self.resetTimer()
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateCountdown), userInfo: nil, repeats: true) //start timer
        
        GameModel.coloursCodeArray.removeAll()
        GameModel.coloursCodeArray = self.gameModel.setCodeArray()
        self.gameModel.actuallyTurn = 0
        
        //remove data from saved
        self.savedPlayersArrayAndTurn = [:]
        self.savedCheckResult = [:]
        
        //remove rows from tableview
        self.objects.removeAll()
        self.colorsTable.reloadData()
        
        self.correctAnsw0Label.layer.backgroundColor = UIColor.clear.cgColor
        self.correctAnsw1Label.layer.backgroundColor = UIColor.clear.cgColor
        self.correctAnsw2Label.layer.backgroundColor = UIColor.clear.cgColor
        self.correctAnsw3Label.layer.backgroundColor = UIColor.clear.cgColor
        
        print("Game started")
        
    }
    
    // MARK: Start new game alertController
    
    func showPlayStartAlert() {
        let newGameAlertController: UIAlertController?
        newGameAlertController = UIAlertController(title: "New Game", message: "Do you want to start new game?", preferredStyle: .alert)
        let noAction = UIAlertAction(title: "No", style: .cancel, handler: {(action) -> Void in print("cancel")
        })
        let yesAction = UIAlertAction(title: "Yes", style: .default, handler: {(action) -> Void in
            self.prepareNewGame()
        })
        newGameAlertController?.addAction(noAction)
        newGameAlertController?.addAction(yesAction)
        if let alertController = newGameAlertController {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    //MARK: Win/lost alertController
   
    
    func showStateGameAlert() {
        let stateGameAlert: UIAlertController?
        var title: String?
        
        if gameModel.isPlayerWinGame() == .youWin {
            title = "Congratulations! You WIN!"
            self.stopTimer()
            showCorrectAnswer()
        } else if gameModel.isPlayerWinGame() == .youLost {
            title = "Sorry... GAME OVER!"
            self.stopTimer()
            showCorrectAnswer()
        } else {
            return
        }
        stateGameAlert = UIAlertController(title: title, message: "Try again?", preferredStyle: .alert)
        let noAction = UIAlertAction(title: "No", style: .cancel, handler: { (action) -> Void in print("cancel")
            })
        let yesAction = UIAlertAction(title: "Yes", style: .default, handler: {(action) -> Void in
            self.prepareNewGame()
        })
        let saveAction = UIAlertAction(title: "Save my result", style: .default, handler: { (action) -> Void  in
            print("save score")
            self.showSaveUserResultAlert()
        })
        stateGameAlert?.addAction(noAction)
        stateGameAlert?.addAction(yesAction)
        if  gameModel.isPlayerWinGame() == .youWin   {
            stateGameAlert?.addAction(saveAction)
        }
        if let alertController = stateGameAlert {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    //MARK: Save player's result AlertController
    
    func showSaveUserResultAlert() {
        let saveUserResultAlert = UIAlertController(title: "Your result: "+self.time.text!, message: "Please enter your name: ", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {(action) in
            print("cancel")
        })
        let okAction = UIAlertAction(title: "OK", style: .default, handler: {(action) in
            let userText = saveUserResultAlert.textFields![0] as UITextField
            
            self.userDataManager.saveUserResultInCoreData(name: userText.text!, timeScore: self.time.text!)
            self.prepareNewGame()
        })
        saveUserResultAlert.addAction(cancelAction)
        saveUserResultAlert.addAction(okAction)
        saveUserResultAlert.addTextField(configurationHandler: {(textFild) in
            textFild.placeholder = "Your name"
        })
        self.present(saveUserResultAlert, animated: true, completion: nil)
    }
    
    func printColor (array: Array<String>, index: Int) -> UIColor {
        switch array[index] {
            case "red":
                return CustomColors.cRed
            case "orange":
                return CustomColors.cOrange
            case "yellow":
                return CustomColors.cYellow
            case "green":
                return CustomColors.cGreen
            case "teal blue":
                return CustomColors.cTealBlue
            case "blue":
                return CustomColors.cBlue
            case "purple":
                return CustomColors.cPurple
            case "pink":
                return CustomColors.cPink
            case "grey":
                return CustomColors.cGrey
            case "brown":
                return CustomColors.cBrown
            default:
                return UIColor.clear
        }
    }
    
    func showCorrectAnswer() {
        correctAnsw0Label.layer.backgroundColor = printColor(array: GameModel.coloursCodeArray, index: 0).cgColor
        correctAnsw1Label.layer.backgroundColor = printColor(array: GameModel.coloursCodeArray, index: 1).cgColor
        correctAnsw2Label.layer.backgroundColor = printColor(array: GameModel.coloursCodeArray, index: 2).cgColor
        correctAnsw3Label.layer.backgroundColor = printColor(array: GameModel.coloursCodeArray, index: 3).cgColor
    }
}



