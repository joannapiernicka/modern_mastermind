//
//  OptionsViewController.swift
//  ModernMastermind
//
//  Created by P Z on 29.05.2017.
//  Copyright © 2017 Joanna Charysz-Piernicka. All rights reserved.
//

import UIKit

class OptionsViewController: UIViewController {
    
    let gradient = CAGradientLayer()
    let color3 = CustomColors.cBlue.cgColor
    let color2 = CustomColors.cTealBlue.cgColor
    let color1 = CustomColors.cPurple.cgColor

    @IBOutlet weak var backgroundImage: UIImageView!
    
    @IBAction func easyButton(_ sender: Any) {
        GameModel.allTurns = 15
    }
    
    @IBAction func mediumButton(_ sender: Any) {
        GameModel.allTurns = 10
    }
    
    @IBAction func hardButton(_ sender: Any) {
       GameModel.allTurns = 8
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        gradient.frame = self.view.bounds
        gradient.colors = [color1, color2, color3]
        gradient.locations = [0.0, 0.5, 1.0]
        self.view.layer.insertSublayer(gradient, at: 0)
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

    
    
    
  

 
