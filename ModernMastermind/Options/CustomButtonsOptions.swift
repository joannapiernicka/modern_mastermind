//
//  CustomButtonsOptions.swift
//  ModernMastermind
//
//  Created by P Z on 31.05.2017.
//  Copyright © 2017 Joanna Charysz-Piernicka. All rights reserved.
//

import UIKit

class CustomButtonsOptions: UIButton {

        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            self.layer.cornerRadius = 8
            self.layer.borderColor = UIColor.white.cgColor
            self.layer.borderWidth = 1
    }
}
