//
//  UserDataManager.swift
//  ModernMastermind
//
//  Created by P Z on 15.05.2017.
//  Copyright © 2017 Joanna Charysz-Piernicka. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class UserDataManager {
    
    var players = [NSManagedObject]()
    
    func saveUserResultInCoreData(name: String, timeScore: String) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Player", in: managedContext)
        let player = NSManagedObject(entity: entity!, insertInto: managedContext)
        player.setValue(name, forKeyPath: "name")
        player.setValue(timeScore, forKeyPath: "timeScore")
        do {
            try managedContext.save()
            players.append(player)
        } catch let error as NSError {
            print("Blad zapisu: \(error)")
        }
    }
    
    func fetchUserResultFromCoreData() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Player")
        let timeScoreSort = NSSortDescriptor(key: "timeScore", ascending: true)
        fetchRequest.sortDescriptors = [timeScoreSort]
        do {
            players = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Blad odczytu: \(error)")
        }
        //delete users result over 20
        if players.count > 20 {
            let index = players.count - 1
            managedContext.delete(players[index])
            print("Index \(index) deleted")
        }
        //and save after deleting
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Blad zapisu: \(error)")
        }
        
    }
    
    func deleteAllUserResultFromCoreData() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Player")
        do {
            players = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Blad odczytu: \(error)")
        }
        for object in players {
        managedContext.delete(object)
        print("All objetcs deleted")
        }
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Blad zapisu: \(error)")
        }
    }
    
    func deleteSingleUserResultFromCoreData(index: Int) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Player")
        do {
            players = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Blad odczytu: \(error)")
        }
        managedContext.delete(players[index]) //index is the same as indexPath.row in tableView
        print("One object deleted")
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Blad zapisu: \(error)")
        }
    }
}
