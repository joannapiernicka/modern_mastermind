//
//  GameManager.swift
//  MastermindGame
//
//  Created by P Z on 28.02.2017.
//  Copyright © 2017 Joanna Charysz-Piernicka.
//

import UIKit

class GameModel {
    
    static let colours = ["red", "orange", "yellow", "green", "teal blue", "blue", "purple", "pink", "grey", "brown"]
    static var codeLenght = 4 //liczba miejsc
    
    static var allTurns = 3// liczba prób
    var actuallyTurn = 0
    
    static var coloursCodeArray = [String]()
    static var coloursPlayersArray = [String]()
    
    var placeAndColourOk = 0  // dobry kolor na dobrym miejscu
    var onlyColourOk = 0 // dobry kolor na złym miejscu
    
    
    //MARK: losowanie koloru
    
    func randomColours()->String {
        let colourIndex = Int(arc4random_uniform(UInt32(GameModel.colours.count)))
        return GameModel.colours[colourIndex]
    }
    
    //MARK: losowanie kodu do odgadniecia
    
    func setCodeArray() -> [String]{
        while GameModel.coloursCodeArray.count < GameModel.codeLenght {
            GameModel.coloursCodeArray.append(randomColours())
        }
        print("Kod do zgadniecia:\(GameModel.coloursCodeArray)") //testowo
        return GameModel.coloursCodeArray
    }
    
    //MARK: porównywanie tabeli gracza z kodem
    
    func check(_: Array<Any>) -> (placeAndColourOk: Int, onlyColourOk: Int, actuallyTurn: Int) {
        var indexTable = 0
        placeAndColourOk = 0
        onlyColourOk = 0
        for _ in GameModel.coloursCodeArray {
            if GameModel.coloursPlayersArray[indexTable] == GameModel.coloursCodeArray[indexTable] {
                print("kolor: ok, miejsce: ok")
                placeAndColourOk += 1
            } else
                 if GameModel.coloursPlayersArray.contains(GameModel.coloursCodeArray[indexTable]) {
                    print("kolor: ok, miejsce: nie")
                    onlyColourOk += 1
                } else {
                    print("kolor: nie, miejsce: nie")
                }
            indexTable += 1
        }
        actuallyTurn += 1
        print("ok kolor i miejsce: \(placeAndColourOk)")
        print("ok tylko kolor: \(onlyColourOk)")
        return (placeAndColourOk, onlyColourOk, actuallyTurn)
    }
    
    //MARK: wynik gry
    
    enum GameState {
        case youWin, youLost, continueGame
    }
    
    func isPlayerWinGame() -> GameState? {
        var gameState : GameState?
        if placeAndColourOk == GameModel.coloursCodeArray.count {
            gameState = .youWin
            print("Wygrałeś")
            return gameState
        } else if actuallyTurn == GameModel.allTurns {
            gameState = .youLost
            print("Przegrałeś")
            return gameState
        } else {
            gameState = .continueGame
            print("Grasz dalej")
            return gameState
        }
    }

    
    init() {
        
     
    }
    
}


