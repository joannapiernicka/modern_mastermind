#Modern Mastermind


iOS app for one player based on a popular code-breaking game. The player can select a level and save score.

----
##screen shots

![Alt text](https://bytebucket.org/joannapiernicka/modern_mastermind/raw/359c3b2691632ab0f11cf05948973a41a1e0ae2f/screenshots/MMScreenOptions.png)

![Alt text](https://bytebucket.org/joannapiernicka/modern_mastermind/raw/359c3b2691632ab0f11cf05948973a41a1e0ae2f/screenshots/MMScreenGame.png)

![Alt text](https://bytebucket.org/joannapiernicka/modern_mastermind/raw/359c3b2691632ab0f11cf05948973a41a1e0ae2f/screenshots/MMScreenSave.png)

![Alt text](https://bytebucket.org/joannapiernicka/modern_mastermind/raw/359c3b2691632ab0f11cf05948973a41a1e0ae2f/screenshots/MMScreenTop.png)

----
##game description
[see Wikipedia](https://en.wikipedia.org/wiki/Mastermind_(board_game)


----
##rules
* code is random from 8 colors
* you must guess code
* you can try 8, 10 or 15 times


----
##used
* Swift 3
* UIKit
* CoreData
* Autolayout
* MVC

----
##license

    Copyright 2017 Joanna Charysz-Piernicka

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software 
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.






